<?php 
require 'header.php';
?>
<html>
    <body class="bg-secondary" style="min-width:300px">

    <div class="container mt-5">
        <div class="row">
            <div class="col-sm d-flex justify-content-center align-items-center">
                <img id="orig" width="700" height="350">
            </div>
            <div class="col-sm d-flex justify-content-center align-items-center">
                <img id="vers"  width="700" height="350">
            </div>
        </div>
    </div>

    <script src="script.js"></script> 
    </body>
</html>