<?php 
require 'header.php';

$fileName = 'subjNr.txt';
$f = fopen($fileName, 'r');

if ($f) {
    $contents = fread($f, filesize($fileName));
}
fclose($f);

if(file_exists($fileName)){
    $myfile = fopen($fileName, "w") or die("Unable to open file!"); 
}
$txt = intval($contents) + 1;
fwrite($myfile, $txt);
fclose($myfile);

?>
<html>
    <body class="bg-secondary" style="min-width:300px">

    <div class="d-flex align-items-center min-vh-100">
        <div class=" container-sm text-center p-4 mt-4 rounded-2 w-50" id="info1">
            <h1 class="text-light">Ronda de Treino</h1>
            <h3 class="text-light">Seram apresentadas duas imagens lado a lado.</h3>
            <h3 class="text-light">A imagem à sua esquerda será a imagem de controlo.</h3>
        </div>
    </div>

    </body>
    <script>
    window.setInterval('changeContent()', 5000);
    window.setInterval('changePage()', 10000); 	

    function changeContent() {
        let info = document.getElementById("info1");
        info.innerHTML = '<h1 class="text-light">Ronda de Treino</h1>'+
            '<h3 class="text-light">O seu objetivo será classificar a imagem da direita em comparação à da esquerda.</h3>'+
            '<h3 class="text-light">A classificação será feita com valores entre 1 a 5.</h3>'+
            '<h3 class="text-light">Deve usar o seu teclado para esse efeito.</h3>';
    }

    function changePage(){
        window.location.replace("screen1.php");
    }
    </script> 
</html>