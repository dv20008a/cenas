<?php
    header('Content-Type: application/json');

    $aResult = array();

    if( !isset($_POST['functionname']) ) { $aResult['error'] = 'No function name!'; }

    if( !isset($_POST['arguments']) ) { $aResult['error'] = 'No function arguments!'; }

    if( !isset($aResult['error']) ) {

        switch($_POST['functionname']) {
            case 'WriteFile':
                $fileName = 'subjNr.txt';
                $f = fopen($fileName, 'r');

                if ($f) {
                    $contents = fread($f, filesize($fileName));
                }
                fclose($f);
                
                $fileName = "subject".$contents.".txt";
                if(file_exists($fileName)){
                    $myfile = fopen($fileName, "a") or die("Unable to open file!");
                }else{
                    $myfile = fopen($fileName, "w") or die("Unable to open file!"); 
                }
                $txt = $_POST['arguments'][1]." ".$_POST['arguments'][0]."\n";
                fwrite($myfile, $txt);
                fclose($myfile);
                
                $aResult['error'] = "success";
               break;

            default:
               $aResult['error'] = 'Not found function '.$_POST['functionname'].'!';
               break;
        }

    }

    echo json_encode($aResult);

?>
