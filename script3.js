let flag = JSON.parse(sessionStorage.getItem("testFlag"));;
let TestSet = JSON.parse(sessionStorage.getItem("TestArray"));
let TestPair = TestSet[flag];
let fileName = TestPair[0]+"_"+TestPair[1]+".txt";

let orig = document.getElementById("orig");
let vers = document.getElementById("vers");

orig.src = "Images/OriginalHdr/" + TestPair[0];
vers.src = "Images/VersionsHdr/" + TestPair[1];

let x = orig.src;

if(x.includes("memorial")){
    orig.width = "380";
    vers.width = "380";
    orig.height = "570";
    vers.height = "570";
}
else{
    orig.width = "540";
    vers.width = "540";
    orig.height = "350";
    vers.height = "350";
}

document.addEventListener('keypress', (event) => {
    var name = event.key;
    var code = event.code;
    let grade;
    
    
    if(code == "Numpad1" || code == "Digit1"){
        grade = 1;
    }else if(code == "Numpad2" || code == "Digit2"){
        grade = 2;
    }else if(code == "Numpad3" || code == "Digit3"){
        grade = 3;
    }else if(code == "Numpad4" || code == "Digit4"){
        grade = 4;
    }else if(code == "Numpad5" || code == "Digit5"){
        grade = 5;
    }else{
        return;
    }
    
    console.log(fileName);

    jQuery.ajax({
        type: "POST",
        url: 'saveFile.php',
        dataType: 'json',
        data: {functionname: 'WriteFile', arguments: [grade,fileName]},
        success: function (result) {
            
            flag++;
            sessionStorage.setItem("testFlag", JSON.stringify(flag));
            TestPair = TestSet[flag];
            fileName = TestPair[0]+"_"+TestPair[1]+".txt";

            if(flag < TestSet.length-1){
                orig.src = "Images/OriginalHdr/" + TestPair[0];
                vers.src = "Images/VersionsHdr/" + TestPair[1];
                 x = orig.src;

                if(x.includes("memorial")){
                    orig.width = "380";
                    vers.width = "380";
                    orig.height = "570";
                    vers.height = "570";
                }
                else{
                    orig.width = "540";
                    vers.width = "540";
                    orig.height = "350";
                    vers.height = "350";
                }

            }else{
                window.location.replace("info4.php");
            } 
        }
    });

  }, false);
