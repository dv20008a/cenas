from math import log10, sqrt
from skimage import io
from os.path import exists
import cv2
import sys
import numpy as np
import matplotlib.pyplot as plt
from skimage.metrics import structural_similarity as ssim
arr_psnr = []
arr_mse = []
arr_ssim = []
horses_jp2_out1 = []
horses_jp2_out2 = []
horses_jp2_out3 = []
horses_jp2_out4 = []
cars_jp2_out1 = []
cars_jp2_out2 = []
cars_jp2_out3 = []
cars_jp2_out4 = []
mountain_jp2_out1 = []
mountain_jp2_out2 = []
mountain_jp2_out3 = []
mountain_jp2_out4 = [] 
teto_jp2_out1 = []
teto_jp2_out2 = []
teto_jp2_out3 = []
teto_jp2_out4 = [] 
#COmment
horses_hvec_out1 = []
horses_hvec_out2 = []
horses_hvec_out3 = []
horses_hvec_out4 = []
cars_hvec_out1 = []
cars_hvec_out2 = []
cars_hvec_out3 = []
cars_hvec_out4 = []
mountain_hvec_out1 = []
mountain_hvec_out2 = []
mountain_hvec_out3 = []
mountain_hvec_out4 = [] 
teto_hvec_out1 = []
teto_hvec_out2 = []
teto_hvec_out3 = []
teto_hvec_out4 = [] 
#Comment
horses_vp9_out1 = []
horses_vp9_out2 = []
horses_vp9_out3 = []
horses_vp9_out4 = []
cars_vp9_out1 = []
cars_vp9_out2 = []
cars_vp9_out3 = []
cars_vp9_out4 = []
mountain_vp9_out1 = []
mountain_vp9_out2 = []
mountain_vp9_out3 = []
mountain_vp9_out4 = [] 
teto_vp9_out1 = []
teto_vp9_out2 = []
teto_vp9_out3 = []
teto_vp9_out4 = [] 


mos_horses_jp2_out1 = 0.0
mos_horses_jp2_out2 = 0.0
mos_horses_jp2_out3 = 0.0
mos_horses_jp2_out4 = 0.0
mos_cars_jp2_out1 = 0.0
mos_cars_jp2_out2 = 0.0
mos_cars_jp2_out3 = 0.0
mos_cars_jp2_out4 = 0.0
mos_mountain_jp2_out1 = 0.0
mos_mountain_jp2_out2 = 0.0
mos_mountain_jp2_out3 = 0.0
mos_mountain_jp2_out4 = 0.0 
mos_teto_jp2_out1 = 0.0
mos_teto_jp2_out2 = 0.0
mos_teto_jp2_out3 = 0.0
mos_teto_jp2_out4 = 0.0 
mos_horses_hvec_out1 = 0.0
mos_horses_hvec_out2 = 0.0
mos_horses_hvec_out3 = 0.0
mos_horses_hvec_out4 = 0.0
mos_cars_hvec_out1 = 0.0
mos_cars_hvec_out2 = 0.0
mos_cars_hvec_out3 = 0.0
mos_cars_hvec_out4 = 0.0
mos_mountain_hvec_out1 = 0.0
mos_mountain_hvec_out2 = 0.0
mos_mountain_hvec_out3 = 0.0
mos_mountain_hvec_out4 = 0.0 
mos_teto_hvec_out1 = 0.0
mos_teto_hvec_out2 = 0.0
mos_teto_hvec_out3 = 0.0
mos_teto_hvec_out4 = 0.0 
mos_horses_vp9_out1 = 0.0
mos_horses_vp9_out2 = 0.0
mos_horses_vp9_out3 = 0.0
mos_horses_vp9_out4 = 0.0
mos_cars_vp9_out1 = 0.0
mos_cars_vp9_out2 = 0.0
mos_cars_vp9_out3 = 0.0
mos_cars_vp9_out4 = 0.0
mos_mountain_vp9_out1 = 0.0
mos_mountain_vp9_out2 = 0.0
mos_mountain_vp9_out3 = 0.0
mos_mountain_vp9_out4 = 0.0 
mos_teto_vp9_out1 = 0.0
mos_teto_vp9_out2 = 0.0
mos_teto_vp9_out3 = 0.0
mos_teto_vp9_out4 = 0.0

def PSNR(original, compressed):
    original = original.astype(np.float64) / 255.
    compressed = compressed.astype(np.float64) / 255.
    mse = np.mean((original - compressed) ** 2)
    if(mse == 0):  # MSE is zero means no noise is present in the signal .
                  # Therefore PSNR have no importance.
        return 100
    max_pixel = 255.0
    psnr = 20 * log10(max_pixel / sqrt(mse))
    return psnr
def mse(imageA, imageB):
    err = np.sum((imageA.astype("float") - imageB.astype("float")) ** 2)
    err /= float(imageA.shape[0] * imageA.shape[1])
    return err
    
def calcular_mos():
    global arr_psnr
    global arr_mse
    global arr_ssim
    global horses_jp2_out1
    global horses_jp2_out2
    global horses_jp2_out3
    global horses_jp2_out4
    global cars_jp2_out1
    global cars_jp2_out2
    global cars_jp2_out3
    global cars_jp2_out4
    global mountain_jp2_out1
    global mountain_jp2_out2
    global mountain_jp2_out3
    global mountain_jp2_out4 
    global teto_jp2_out1
    global teto_jp2_out2
    global teto_jp2_out3
    global teto_jp2_out4 
    #COmment
    global horses_hvec_out1
    global horses_hvec_out2
    global horses_hvec_out3
    global horses_hvec_out4
    global cars_hvec_out1
    global cars_hvec_out2
    global cars_hvec_out3
    global cars_hvec_out4
    global mountain_hvec_out1
    global mountain_hvec_out2
    global mountain_hvec_out3
    global mountain_hvec_out4
    global teto_hvec_out1
    global teto_hvec_out2
    global teto_hvec_out3
    global teto_hvec_out4
    #Comment
    global horses_vp9_out1
    global horses_vp9_out2
    global horses_vp9_out3
    global horses_vp9_out4
    global cars_vp9_out1
    global cars_vp9_out2
    global cars_vp9_out3
    global cars_vp9_out4
    global mountain_vp9_out1
    global mountain_vp9_out2
    global mountain_vp9_out3
    global mountain_vp9_out4 
    global teto_vp9_out1
    global teto_vp9_out2
    global teto_vp9_out3
    global teto_vp9_out4 
    global mos_horses_jp2_out1 
    global mos_horses_jp2_out2 
    global mos_horses_jp2_out3 
    global mos_horses_jp2_out4 
    global mos_cars_jp2_out1 
    global mos_cars_jp2_out2 
    global mos_cars_jp2_out3 
    global mos_cars_jp2_out4 
    global mos_mountain_jp2_out1 
    global mos_mountain_jp2_out2 
    global mos_mountain_jp2_out3 
    global mos_mountain_jp2_out4  
    global mos_teto_jp2_out1 
    global mos_teto_jp2_out2 
    global mos_teto_jp2_out3 
    global mos_teto_jp2_out4  
    global mos_horses_hvec_out1 
    global mos_horses_hvec_out2 
    global mos_horses_hvec_out3 
    global mos_horses_hvec_out4 
    global mos_cars_hvec_out1 
    global mos_cars_hvec_out2 
    global mos_cars_hvec_out3 
    global mos_cars_hvec_out4 
    global mos_mountain_hvec_out1 
    global mos_mountain_hvec_out2 
    global mos_mountain_hvec_out3 
    global mos_mountain_hvec_out4  
    global mos_teto_hvec_out1 
    global mos_teto_hvec_out2 
    global mos_teto_hvec_out3 
    global mos_teto_hvec_out4  
    global mos_horses_vp9_out1 
    global mos_horses_vp9_out2 
    global mos_horses_vp9_out3 
    global mos_horses_vp9_out4 
    global mos_cars_vp9_out1 
    global mos_cars_vp9_out2 
    global mos_cars_vp9_out3 
    global mos_cars_vp9_out4 
    global mos_mountain_vp9_out1 
    global mos_mountain_vp9_out2 
    global mos_mountain_vp9_out3 
    global mos_mountain_vp9_out4  
    global mos_teto_vp9_out1 
    global mos_teto_vp9_out2 
    global mos_teto_vp9_out3 
    global mos_teto_vp9_out4 
    for i in range(1,17):
        i_s = str(i)
        name_file = "testes/subject"+i_s+".txt"
        with open(name_file,'r') as data_file:
            for line in data_file:
                data = line.split()
                if(data[0] == 'horses.png_horseshvec_out1.png.txt'):
                        horses_hvec_out1.append(int(data[1]))
                elif(data[0] == 'horses.png_horseshvec_out2.png.txt'):
                        horses_hvec_out2.append(int(data[1]))
                elif(data[0] == 'horses.png_horseshvec_out3.png.txt'):
                        horses_hvec_out3.append(int(data[1]))
                elif(data[0] == 'horses.png_horseshvec_out4.png.txt'):
                        horses_hvec_out4.append(int(data[1])) 
                elif(data[0] == 'cars.png_carshvec_out1.png.txt'):
                        cars_hvec_out1.append(int(data[1]))
                elif(data[0] == 'cars.png_carshvec_out2.png.txt'):
                        cars_hvec_out2.append(int(data[1]))
                elif(data[0] == 'cars.png_carshvec_out3.png.txt'):
                        cars_hvec_out3.append(int(data[1]))
                elif(data[0] == 'cars.png_carshvec_out4.png.txt'):
                        cars_hvec_out4.append(int(data[1]))   
                elif(data[0] == 'mountain.png_mountainhvec_out1.png.txt'):
                        mountain_hvec_out1.append(int(data[1]))
                elif(data[0] == 'mountain.png_mountainhvec_out2.png.txt'):
                        mountain_hvec_out2.append(int(data[1]))
                elif(data[0] == 'mountain.png_mountainhvec_out3.png.txt'):
                        mountain_hvec_out3.append(int(data[1]))
                elif(data[0] == 'mountain.png_mountainhvec_out4.png.txt'):
                        mountain_hvec_out4.append(int(data[1]))
                elif(data[0] == 'teto.png_tetohvec_out1.png.txt'):
                        print('Entrou')
                        teto_hvec_out1.append(int(data[1]))
                elif(data[0] == 'teto.png_tetohvec_out2.png.txt'):
                        teto_hvec_out2.append(int(data[1]))
                elif(data[0] == 'teto.png_tetohvec_out3.png.txt'):
                        teto_hvec_out3.append(int(data[1]))
                elif(data[0] == 'teto.png_tetohvec_out4.png.txt'):
                        teto_hvec_out4.append(int(data[1]))   #FIM DO hvec
                elif(data[0] == 'horses.png_horsesjp2_out1.png.txt'):
                        horses_jp2_out1.append(int(data[1]))
                elif(data[0] == 'horses.png_horsesjp2_out2.png.txt'):
                        horses_jp2_out2.append(int(data[1]))
                elif(data[0] == 'horses.png_horsesjp2_out3.png.txt'):
                        horses_jp2_out3.append(int(data[1]))
                elif(data[0] == 'horses.png_horsesjp2_out4.png.txt'):
                        horses_jp2_out4.append(int(data[1])) 
                elif(data[0] == 'cars.png_carsjp2_out1.png.txt'):
                        cars_jp2_out1.append(int(data[1]))
                elif(data[0] == 'cars.png_carsjp2_out2.png.txt'):
                        cars_jp2_out2.append(int(data[1]))
                elif(data[0] == 'cars.png_carsjp2_out3.png.txt'):
                        cars_jp2_out3.append(int(data[1]))
                elif(data[0] == 'cars.png_carsjp2_out4.png.txt'):
                        cars_jp2_out4.append(int(data[1]))   
                elif(data[0] == 'mountain.png_mountainjp2_out1.png.txt'):
                        mountain_jp2_out1.append(int(data[1]))
                elif(data[0] == 'mountain.png_mountainjp2_out2.png.txt'):
                        mountain_jp2_out2.append(int(data[1]))
                elif(data[0] == 'mountain.png_mountainjp2_out3.png.txt'):
                        mountain_jp2_out3.append(int(data[1]))
                elif(data[0] == 'mountain.png_mountainjp2_out4.png.txt'):
                        mountain_jp2_out4.append(int(data[1]))
                elif(data[0] == 'teto.png_tetojp2_out1.png.txt'):
                        teto_jp2_out1.append(int(data[1]))
                elif(data[0] == 'teto.png_tetojp2_out2.png.txt'):
                        teto_jp2_out2.append(int(data[1]))
                elif(data[0] == 'teto.png_tetojp2_out3.png.txt'):
                        teto_jp2_out3.append(int(data[1]))
                elif(data[0] == 'teto.png_tetojp2_out4.png.txt'):
                        teto_jp2_out4.append(int(data[1]))     #FIM do JP2
                elif(data[0] == 'horses.png_horsesvp9_out1.png.txt'):
                        horses_vp9_out1.append(int(data[1]))
                elif(data[0] == 'horses.png_horsesvp9_out2.png.txt'):
                        horses_vp9_out2.append(int(data[1]))
                elif(data[0] == 'horses.png_horsesvp9_out3.png.txt'):
                        horses_vp9_out3.append(int(data[1]))
                elif(data[0] == 'horses.png_horsesvp9_out4.png.txt'):
                        horses_vp9_out4.append(int(data[1])) 
                elif(data[0] == 'cars.png_carsvp9_out1.png.txt'):
                        cars_vp9_out1.append(int(data[1]))
                elif(data[0] == 'cars.png_carsvp9_out2.png.txt'):
                        cars_vp9_out2.append(int(data[1]))
                elif(data[0] == 'cars.png_carsvp9_out3.png.txt'):
                        cars_vp9_out3.append(int(data[1]))
                elif(data[0] == 'cars.png_carsvp9_out4.png.txt'):
                        cars_vp9_out4.append(int(data[1]))   
                elif(data[0] == 'mountain.png_mountainvp9_out1.png.txt'):
                        mountain_vp9_out1.append(int(data[1]))
                elif(data[0] == 'mountain.png_mountainvp9_out2.png.txt'):
                        mountain_vp9_out2.append(int(data[1]))
                elif(data[0] == 'mountain.png_mountainvp9_out3.png.txt'):
                        mountain_vp9_out3.append(int(data[1]))
                elif(data[0] == 'mountain.png_mountainvp9_out4.png.txt'):
                        mountain_vp9_out4.append(int(data[1]))
                elif(data[0] == 'teto.png_tetovp9_out1.png.txt'):
                        teto_vp9_out1.append(int(data[1]))
                elif(data[0] == 'teto.png_tetovp9_out2.png.txt'):
                        teto_vp9_out2.append(int(data[1]))
                elif(data[0] == 'teto.png_tetovp9_out3.png.txt'):
                        teto_vp9_out3.append(int(data[1]))
                elif(data[0] == 'teto.png_tetovp9_out4.png.txt'):
                        teto_vp9_out4.append(int(int(data[1])))  
        mos_horses_jp2_out1 = np.mean(horses_jp2_out1) 
        mos_horses_jp2_out2 = np.mean(horses_jp2_out2) 
        mos_horses_jp2_out3 = np.mean(horses_jp2_out3) 
        mos_horses_jp2_out4 = np.mean(horses_jp2_out4) 
        mos_cars_jp2_out1 = np.mean(cars_jp2_out1) 
        mos_cars_jp2_out2 = np.mean(cars_jp2_out2) 
        mos_cars_jp2_out3 = np.mean(cars_jp2_out3) 
        mos_cars_jp2_out4 = np.mean(cars_jp2_out4) 
        mos_mountain_jp2_out1 =  np.mean(mountain_jp2_out1) 
        mos_mountain_jp2_out2 =  np.mean(mountain_jp2_out2) 
        mos_mountain_jp2_out3 =  np.mean(mountain_jp2_out3) 
        mos_mountain_jp2_out4 =  np.mean(mountain_jp2_out4) 
        mos_teto_jp2_out1 =  np.mean(teto_jp2_out1) 
        mos_teto_jp2_out2 =  np.mean(teto_jp2_out2) 
        mos_teto_jp2_out3 =  np.mean(teto_jp2_out3) 
        mos_teto_jp2_out4 =  np.mean(teto_jp2_out4) 
        mos_horses_hvec_out1 = np.mean(horses_hvec_out1) 
        mos_horses_hvec_out2 = np.mean(horses_hvec_out2) 
        mos_horses_hvec_out3 = np.mean(horses_hvec_out3) 
        mos_horses_hvec_out4 = np.mean(horses_hvec_out4) 
        mos_cars_hvec_out1 = np.mean(cars_hvec_out1) 
        mos_cars_hvec_out2 = np.mean(cars_hvec_out2) 
        mos_cars_hvec_out3 = np.mean(cars_hvec_out3) 
        mos_cars_hvec_out4 = np.mean(cars_hvec_out4) 
        mos_mountain_hvec_out1 =  np.mean(mountain_hvec_out1) 
        mos_mountain_hvec_out2 =  np.mean(mountain_hvec_out2) 
        mos_mountain_hvec_out3 =  np.mean(mountain_hvec_out3) 
        mos_mountain_hvec_out4 =  np.mean(mountain_hvec_out4) 
        mos_teto_hvec_out1 =  np.mean(teto_hvec_out1) 
        mos_teto_hvec_out2 =  np.mean(teto_hvec_out2) 
        mos_teto_hvec_out3 =  np.mean(teto_hvec_out3) 
        mos_teto_hvec_out4 =  np.mean(teto_hvec_out4) 
        mos_horses_vp9_out1 = np.mean(horses_vp9_out1) 
        mos_horses_vp9_out2 = np.mean(horses_vp9_out2) 
        mos_horses_vp9_out3 = np.mean(horses_vp9_out3) 
        mos_horses_vp9_out4 = np.mean(horses_vp9_out4) 
        mos_cars_vp9_out1 = np.mean(cars_vp9_out1) 
        mos_cars_vp9_out2 = np.mean(cars_vp9_out2) 
        mos_cars_vp9_out3 = np.mean(cars_vp9_out3) 
        mos_cars_vp9_out4 = np.mean(cars_vp9_out4) 
        mos_mountain_vp9_out1 =  np.mean(mountain_vp9_out1) 
        mos_mountain_vp9_out2 =  np.mean(mountain_vp9_out2) 
        mos_mountain_vp9_out3 =  np.mean(mountain_vp9_out3) 
        mos_mountain_vp9_out4 =  np.mean(mountain_vp9_out4) 
        mos_teto_vp9_out1 =  np.mean(teto_vp9_out1) 
        mos_teto_vp9_out2 =  np.mean(teto_vp9_out2) 
        mos_teto_vp9_out3 =  np.mean(teto_vp9_out3) 
        mos_teto_vp9_out4 =  np.mean(teto_vp9_out4)         
    
def make_graphs_psnr():
    plt.clf()
    global arr_psnr
    x = [arr_psnr[0],arr_psnr[1],arr_psnr[2],arr_psnr[3]]
    y = [mos_horses_jp2_out1,mos_horses_jp2_out2,mos_horses_jp2_out3,mos_horses_jp2_out4]
    plt.plot(x, y)
    plt.title('Horses PSNR JP2!')
    plt.show()  
    plt.savefig('graficos/horses_psnr_jp2.png')
    plt.clf()
    x1 = [arr_psnr[4],arr_psnr[5],arr_psnr[6],arr_psnr[7]]
    y1 = [mos_cars_jp2_out1,mos_cars_jp2_out2,mos_cars_jp2_out3,mos_cars_jp2_out4]
    plt.plot(x1, y1)
    plt.title('Cars PSNR JP2!!')
    plt.show()  
    plt.savefig('graficos/cars_psnr_jp2.png')
    plt.clf()
    x2 = [arr_psnr[8],arr_psnr[9],arr_psnr[10],arr_psnr[11]]
    y2 = [mos_mountain_jp2_out1,mos_mountain_jp2_out2,mos_mountain_jp2_out3,mos_mountain_jp2_out4]
    plt.plot(x2, y2)
    plt.title('Mountain PSNR JP2!')
    plt.show()  
    plt.savefig('graficos/mountain_psnr_jp2.png')  
    plt.clf()    
    x3 = [arr_psnr[12],arr_psnr[13],arr_psnr[14],arr_psnr[15]]
    y3 = [mos_teto_jp2_out1,mos_teto_jp2_out2,mos_teto_jp2_out3,mos_teto_jp2_out4]
    plt.plot(x3, y3)
    plt.title('Teto PSNR JP2!')
    plt.show()  
    plt.savefig('graficos/teto_psnr_jp2.png') 
    plt.clf()
    x4 = [arr_psnr[16],arr_psnr[17],arr_psnr[18],arr_psnr[19]]
    y4 = [mos_horses_vp9_out1,mos_horses_vp9_out2,mos_horses_vp9_out3,mos_horses_vp9_out4]
    plt.plot(x4, y4)
    plt.title('Horses VP9 PSNR!')
    plt.show()  
    plt.savefig('graficos/horses_psnr_vp9.png')
    plt.clf()
    x5 = [arr_psnr[20],arr_psnr[21],arr_psnr[22],arr_psnr[23]]
    y5 = [mos_cars_vp9_out1,mos_cars_vp9_out2,mos_cars_vp9_out3,mos_cars_vp9_out4]
    plt.plot(x5, y5)
    plt.title('Cars VP9 PSNR!')
    plt.show()  
    plt.savefig('graficos/cars_psnr_vp9.png') 
    plt.clf()
    x6 = [arr_psnr[24],arr_psnr[25],arr_psnr[26],arr_psnr[27]]
    y6 = [mos_mountain_vp9_out1,mos_mountain_vp9_out2,mos_mountain_vp9_out3,mos_mountain_vp9_out4]
    plt.plot(x6, y6)
    plt.title('Mountain VP9 PSNR!')
    plt.show()  
    plt.savefig('graficos/mountain_psnr_vp9.png') 
    plt.clf()
    x7 = [arr_psnr[28],arr_psnr[29],arr_psnr[30],arr_psnr[31]]
    y7 = [mos_teto_vp9_out1,mos_teto_vp9_out2,mos_teto_vp9_out3,mos_teto_vp9_out4]
    plt.plot(x7, y7)
    plt.title('Teto PSNR VP9!')
    plt.show()  
    plt.savefig('graficos/teto_psnr_vp9.png')   #AQUI   
    plt.clf()    
    x8 = [arr_psnr[32],arr_psnr[33],arr_psnr[34],arr_psnr[35]]
    y8 = [mos_horses_hvec_out1,mos_horses_hvec_out2,mos_horses_hvec_out3,mos_horses_hvec_out4]
    plt.plot(x8, y8)
    plt.title('Horses hvec PSNR!')
    plt.show()  
    plt.savefig('graficos/horses_psnr_hvec.png')
    plt.clf()
    x9 = [arr_psnr[36],arr_psnr[37],arr_psnr[38],arr_psnr[39]]
    y9 = [mos_cars_hvec_out1,mos_cars_hvec_out2,mos_cars_hvec_out3,mos_cars_hvec_out4]
    plt.plot(x9, y9)
    plt.title('Cars hvec PSNR!')
    plt.show()  
    plt.savefig('graficos/cars_psnr_hvec.png')
    plt.clf()
    x10 = [arr_psnr[40],arr_psnr[41],arr_psnr[42],arr_psnr[43]]
    y10 = [mos_mountain_hvec_out1,mos_mountain_hvec_out2,mos_mountain_hvec_out3,mos_mountain_hvec_out4]
    plt.plot(x10, y10)
    plt.title('Mountain hvec PSNR!')
    plt.show()  
    plt.savefig('graficos/mountain_psnr_hvec.png')
    plt.clf()
    x11 = [arr_psnr[44],arr_psnr[45],arr_psnr[46],arr_psnr[47]]
    y11 = [mos_teto_hvec_out1,mos_teto_hvec_out2,mos_teto_hvec_out3,mos_teto_hvec_out4]
    plt.plot(x11, y11)
    plt.title('Teto PSNR hvec!')
    plt.show()  
    plt.savefig('graficos/teto_psnr_hvec.png')   #AQUI 
def make_graphs_ssim():
    plt.clf()
    global arr_ssim
    x = [arr_ssim[0],arr_ssim[1],arr_ssim[2],arr_ssim[3]]
    y = [mos_horses_jp2_out1,mos_horses_jp2_out2,mos_horses_jp2_out3,mos_horses_jp2_out4]
    plt.plot(x, y)
    plt.title('Horses SSIM JP2!')
    plt.show()  
    plt.savefig('graficos/horses_SSIM_jp2.png')
    plt.clf()
    x1 = [arr_ssim[4],arr_ssim[5],arr_ssim[6],arr_ssim[7]]
    y1 = [mos_cars_jp2_out1,mos_cars_jp2_out2,mos_cars_jp2_out3,mos_cars_jp2_out4]
    plt.plot(x1, y1)
    plt.title('Cars SSIM JP2!!')
    plt.show()  
    plt.savefig('graficos/cars_SSIM_jp2.png')
    plt.clf()
    x2 = [arr_ssim[8],arr_ssim[9],arr_ssim[10],arr_ssim[11]]
    y2 = [mos_mountain_jp2_out1,mos_mountain_jp2_out2,mos_mountain_jp2_out3,mos_mountain_jp2_out4]
    plt.plot(x2, y2)
    plt.title('Mountain SSIM JP2!')
    plt.show()  
    plt.savefig('graficos/mountain_SSIM_jp2.png')  
    plt.clf()    
    x3 = [arr_ssim[12],arr_ssim[13],arr_ssim[14],arr_ssim[15]]
    y3 = [mos_teto_jp2_out1,mos_teto_jp2_out2,mos_teto_jp2_out3,mos_teto_jp2_out4]
    plt.plot(x3, y3)
    plt.title('Teto SSIM JP2!')
    plt.show()  
    plt.savefig('graficos/teto_SSIM_jp2.png') 
    plt.clf()
    x4 = [arr_ssim[16],arr_ssim[17],arr_ssim[18],arr_ssim[19]]
    y4 = [mos_horses_vp9_out1,mos_horses_vp9_out2,mos_horses_vp9_out3,mos_horses_vp9_out4]
    plt.plot(x4, y4)
    plt.title('Horses VP9 SSIM!')
    plt.show()  
    plt.savefig('graficos/horses_SSIM_vp9.png')
    plt.clf()
    x5 = [arr_ssim[20],arr_ssim[21],arr_ssim[22],arr_ssim[23]]
    y5 = [mos_cars_vp9_out1,mos_cars_vp9_out2,mos_cars_vp9_out3,mos_cars_vp9_out4]
    plt.plot(x5, y5)
    plt.title('Cars VP9 SSIM!')
    plt.show()  
    plt.savefig('graficos/cars_SSIM_vp9.png') 
    plt.clf()
    x6 = [arr_ssim[24],arr_ssim[25],arr_ssim[26],arr_ssim[27]]
    y6 = [mos_mountain_vp9_out1,mos_mountain_vp9_out2,mos_mountain_vp9_out3,mos_mountain_vp9_out4]
    plt.plot(x6, y6)
    plt.title('Mountain VP9 SSIM!')
    plt.show()  
    plt.savefig('graficos/mountain_SSIM_vp9.png') 
    plt.clf()
    x7 = [arr_ssim[28],arr_ssim[29],arr_ssim[30],arr_ssim[31]]
    y7 = [mos_teto_vp9_out1,mos_teto_vp9_out2,mos_teto_vp9_out3,mos_teto_vp9_out4]
    plt.plot(x7, y7)
    plt.title('Teto SSIM VP9!')
    plt.show()  
    plt.savefig('graficos/teto_SSIM_vp9.png')   #AQUI   
    plt.clf()    
    x8 = [arr_ssim[32],arr_ssim[33],arr_ssim[34],arr_ssim[35]]
    y8 = [mos_horses_hvec_out1,mos_horses_hvec_out2,mos_horses_hvec_out3,mos_horses_hvec_out4]
    plt.plot(x8, y8)
    plt.title('Horses hvec SSIM!')
    plt.show()  
    plt.savefig('graficos/horses_SSIM_hvec.png')
    plt.clf()
    x9 = [arr_ssim[36],arr_ssim[37],arr_ssim[38],arr_ssim[39]]
    y9 = [mos_cars_hvec_out1,mos_cars_hvec_out2,mos_cars_hvec_out3,mos_cars_hvec_out4]
    plt.plot(x9, y9)
    plt.title('Cars hvec SSIM!')
    plt.show()  
    plt.savefig('graficos/cars_SSIM_hvec.png')
    plt.clf()
    x10 = [arr_ssim[40],arr_ssim[41],arr_ssim[42],arr_ssim[43]]
    y10 = [mos_mountain_hvec_out1,mos_mountain_hvec_out2,mos_mountain_hvec_out3,mos_mountain_hvec_out4]
    plt.plot(x10, y10)
    plt.title('Mountain hvec SSIM!')
    plt.show()  
    plt.savefig('graficos/mountain_SSIM_hvec.png')
    plt.clf()
    x11 = [arr_ssim[44],arr_ssim[45],arr_ssim[46],arr_ssim[47]]
    y11 = [mos_teto_hvec_out1,mos_teto_hvec_out2,mos_teto_hvec_out3,mos_teto_hvec_out4]
    plt.plot(x11, y11)
    plt.title('Teto SSIM hvec!')
    plt.show()  
    plt.savefig('graficos/teto_SSIM_hvec.png')   #AQUI 
def make_graphs_mse():
    global arr_mse
    plt.clf()
    x = [arr_mse[0],arr_mse[1],arr_mse[2],arr_mse[3]]
    y = [mos_horses_jp2_out1,mos_horses_jp2_out2,mos_horses_jp2_out3,mos_horses_jp2_out4]
    plt.plot(x, y)
    plt.title('Horses MSE JP2!')
    plt.show()  
    plt.savefig('graficos/horses_MSE_jp2.png')
    plt.clf()
    x1 = [arr_mse[4],arr_mse[5],arr_mse[6],arr_mse[7]]
    y1 = [mos_cars_jp2_out1,mos_cars_jp2_out2,mos_cars_jp2_out3,mos_cars_jp2_out4]
    plt.plot(x1, y1)
    plt.title('Cars MSE JP2!!')
    plt.show()  
    plt.savefig('graficos/cars_MSE_jp2.png')
    plt.clf()
    x2 = [arr_mse[8],arr_mse[9],arr_mse[10],arr_mse[11]]
    y2 = [mos_mountain_jp2_out1,mos_mountain_jp2_out2,mos_mountain_jp2_out3,mos_mountain_jp2_out4]
    plt.plot(x2, y2)
    plt.title('Mountain MSE JP2!')
    plt.show()  
    plt.savefig('graficos/mountain_MSE_jp2.png')  
    plt.clf()    
    x3 = [arr_mse[12],arr_mse[13],arr_mse[14],arr_mse[15]]
    y3 = [mos_teto_jp2_out1,mos_teto_jp2_out2,mos_teto_jp2_out3,mos_teto_jp2_out4]
    plt.plot(x3, y3)
    plt.title('Teto MSE JP2!')
    plt.show()  
    plt.savefig('graficos/teto_MSE_jp2.png') 
    plt.clf()
    x4 = [arr_mse[16],arr_mse[17],arr_mse[18],arr_mse[19]]
    y4 = [mos_horses_vp9_out1,mos_horses_vp9_out2,mos_horses_vp9_out3,mos_horses_vp9_out4]
    plt.plot(x4, y4)
    plt.title('Horses VP9 MSE!')
    plt.show()  
    plt.savefig('graficos/horses_MSE_vp9.png')
    plt.clf()
    x5 = [arr_mse[20],arr_mse[21],arr_mse[22],arr_mse[23]]
    y5 = [mos_cars_vp9_out1,mos_cars_vp9_out2,mos_cars_vp9_out3,mos_cars_vp9_out4]
    plt.plot(x5, y5)
    plt.title('Cars VP9 MSE!')
    plt.show()  
    plt.savefig('graficos/cars_MSE_vp9.png') 
    plt.clf()
    x6 = [arr_mse[24],arr_mse[25],arr_mse[26],arr_mse[27]]
    y6 = [mos_mountain_vp9_out1,mos_mountain_vp9_out2,mos_mountain_vp9_out3,mos_mountain_vp9_out4]
    plt.plot(x6, y6)
    plt.title('Mountain VP9 MSE!')
    plt.show()  
    plt.savefig('graficos/mountain_MSE_vp9.png') 
    plt.clf()
    x7 = [arr_mse[28],arr_mse[29],arr_mse[30],arr_mse[31]]
    y7 = [mos_teto_vp9_out1,mos_teto_vp9_out2,mos_teto_vp9_out3,mos_teto_vp9_out4]
    plt.plot(x7, y7)
    plt.title('Teto MSE VP9!')
    plt.show()  
    plt.savefig('graficos/teto_MSE_vp9.png')   #AQUI   
    plt.clf()    
    x8 = [arr_mse[32],arr_mse[33],arr_mse[34],arr_mse[35]]
    y8 = [mos_horses_hvec_out1,mos_horses_hvec_out2,mos_horses_hvec_out3,mos_horses_hvec_out4]
    plt.plot(x8, y8)
    plt.title('Horses hvec MSE!')
    plt.show()  
    plt.savefig('graficos/horses_MSE_hvec.png')
    plt.clf()
    x9 = [arr_mse[36],arr_mse[37],arr_mse[38],arr_mse[39]]
    y9 = [mos_cars_hvec_out1,mos_cars_hvec_out2,mos_cars_hvec_out3,mos_cars_hvec_out4]
    plt.plot(x9, y9)
    plt.title('Cars hvec MSE!')
    plt.show()  
    plt.savefig('graficos/cars_MSE_hvec.png')
    plt.clf()
    x10 = [arr_mse[40],arr_mse[41],arr_mse[42],arr_mse[43]]
    y10 = [mos_mountain_hvec_out1,mos_mountain_hvec_out2,mos_mountain_hvec_out3,mos_mountain_hvec_out4]
    plt.plot(x10, y10)
    plt.title('Mountain hvec MSE!')
    plt.show()  
    plt.savefig('graficos/mountain_MSE_hvec.png')
    plt.clf()
    x11 = [arr_mse[44],arr_mse[45],arr_mse[46],arr_mse[47]]
    y11 = [mos_teto_hvec_out1,mos_teto_hvec_out2,mos_teto_hvec_out3,mos_teto_hvec_out4]
    plt.plot(x11, y11)
    plt.title('Teto MSE hvec!')
    plt.show()  
    plt.savefig('graficos/teto_MSE_hvec.png')   #AQUI  
    
def main():
    global arr_psnr
    f = open("res_psnr.txt","w")
    f1 = open("res_mse.txt","w")   
    f2 = open("res_ssim.txt","w")       
    pastas = ["jp2","vp9","hevc"]
    for x in pastas :
        if x == "vp9" :
            ext = "mp4"
        else :
            ext = x
        for i in range (1,5) :
            name1 = x+"/img1.png"
            original = cv2.imread(name1)
            i2 = str(i)
            name = x+"/img1_out"+i2+"."+ext
            print(name)
            if ext == 'mp4' or ext == 'hevc':
                compressed = cv2.VideoCapture(name)
                ret, compressed = compressed.read()
            else:
                compressed = cv2.imread(name)
            value = PSNR(original, compressed)
            value_mse = mse(original, compressed)
            gray1 = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)
            gray2 = cv2.cvtColor(compressed, cv2.COLOR_BGR2GRAY)
            value_ssim = ssim(gray1, gray2)
            arr_psnr.append(format(value, '.2f'))
            arr_mse.append(format(value_mse, '.2f'))
            arr_ssim.append(format(value_ssim, '.2f'))
            value = str(value)
            value_ssim = str(value_ssim)
            value_mse = str(value_mse)
            st_p2 = "SSIM for: "+x+" in horses, out: "+i2+", valor: "+value_ssim+"\n"
            st_p = "PSNR for: "+x+" in horses,out: "+i2+", valor: "+value+"\n"
            st_p1 = "MSE for: "+x+" in horses out: "+i2+", valor: "+value_mse+"\n"
            f.write(st_p)
            f1.write(st_p1)
            f2.write(st_p2)
           # print(st_p)
        for i in range (1,5) :
            name2 = x+"/img2.png"
            original = cv2.imread(name2)
            i2 = str(i)
            name = x+"/img2_out"+i2+"."+ext
            if ext == 'mp4' or ext == 'hevc' :
                compressed = cv2.VideoCapture(name)
                ret, compressed = compressed.read()
            else:
                compressed = cv2.imread(name, 1)
            value = PSNR(original, compressed)
            value_mse = mse(original, compressed)
            gray1 = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)
            gray2 = cv2.cvtColor(compressed, cv2.COLOR_BGR2GRAY)
            value_ssim = ssim(gray1, gray2)
            arr_psnr.append(format(value, '.2f'))
            arr_mse.append(format(value_mse, '.2f'))
            arr_ssim.append(format(value_ssim, '.2f'))
            value = str(value)
            value_ssim = str(value_ssim)
            value_mse = str(value_mse)  
            st_p2 = "SSIM for: "+x+" in cars, out: "+i2+", valor: "+value_ssim+"\n"
            st_p = "PSNR for: "+x+" in cars,out: "+i2+", valor: "+value+"\n"
            st_p1 = "MSE for: "+x+" in cars, out: "+i2+", valor: "+value_mse+"\n"
            f.write(st_p)
            f1.write(st_p1)
            f2.write(st_p2)
        for i in range (1,5) :
            name3 = x+"/img3.png"
            original = cv2.imread(name3)
            i2 = str(i)
            name = x+"/img3_out"+i2+"."+ext
            if ext == 'mp4' or ext == 'hevc':
                compressed = cv2.VideoCapture(name)
                ret, compressed = compressed.read()
            else:
                compressed = cv2.imread(name, 1)
            value = PSNR(original, compressed)
            value_mse = mse(original, compressed)
            gray1 = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)
            gray2 = cv2.cvtColor(compressed, cv2.COLOR_BGR2GRAY)
            value_ssim = ssim(gray1, gray2)
            arr_psnr.append(format(value, '.2f'))
            arr_mse.append(format(value_mse, '.2f'))
            arr_ssim.append(format(value_ssim, '.2f'))
            value = str(value)
            value_ssim = str(value_ssim)
            value_mse = str(value_mse)
            st_p2 = "SSIM for: "+x+" in mountain, out: "+i2+", valor: "+value_ssim+"\n"
            st_p = "PSNR for: "+x+" in mountain, out: "+i2+", valor: "+value+"\n"
            st_p1 = "MSE for: "+x+" in mountain, out: "+i2+", valor: "+value_mse+"\n"
            f.write(st_p)
            f1.write(st_p1)
            f2.write(st_p2)
        for i in range (1,5) :
            name3 = x+"/img4.png"
            original = cv2.imread(name3)
            i2 = str(i)
            name = x+"/img4_out"+i2+"."+ext
            if ext == 'mp4' or ext == 'hevc':
                compressed = cv2.VideoCapture(name)
                ret, compressed = compressed.read()
            else:
                compressed = cv2.imread(name, 1)
            value = PSNR(original, compressed)
            value_mse = mse(original, compressed)
            gray1 = cv2.cvtColor(original, cv2.COLOR_BGR2GRAY)
            gray2 = cv2.cvtColor(compressed, cv2.COLOR_BGR2GRAY)
            value_ssim = ssim(gray1, gray2)
            arr_psnr.append(format(value, '.2f'))
            arr_mse.append(format(value_mse, '.2f'))
            arr_ssim.append(format(value_ssim, '.2f'))
            value = str(value)
            value_ssim = str(value_ssim)
            value_mse = str(value_mse)
            st_p2 = "SSIM for: "+x+" in teto, out: "+i2+", valor: "+value_ssim+"\n"
            st_p = "PSNR for: "+x+" in teto, out: "+i2+", valor: "+value+"\n"
            st_p1 = "MSE for: "+x+" in teto, out: "+i2+", valor: "+value_mse+"\n"
            f.write(st_p)
            f1.write(st_p1)
            f2.write(st_p2)            
    f.close()
    f1.close()
    f2.close()
    calcular_mos()
if __name__ == "__main__":
    main()
    calcular_mos()
    make_graphs_psnr()
    make_graphs_ssim()
    make_graphs_mse()
