let flag = JSON.parse(sessionStorage.getItem("trainingFlag"));;
let TrainingSet = JSON.parse(sessionStorage.getItem("TrainingArray"));
let TrainingPair = TrainingSet[flag];

let orig = document.getElementById("orig");
let vers = document.getElementById("vers");

console.log(TrainingPair);
orig.src = "Images/Original/" + TrainingPair[0];
vers.src = "Images/Versions/" + TrainingPair[1];

document.addEventListener('keypress', (event) => {
    var name = event.key;
    var code = event.code;
    let grade;

    if(code == "Numpad1" || code == "Digit1"){
        grade = 1;
    }else if(code == "Numpad2" || code == "Digit2"){
        grade = 2;
    }else if(code == "Numpad3" || code == "Digit3"){
        grade = 3;
    }else if(code == "Numpad4" || code == "Digit4"){
        grade = 4;
    }else if(code == "Numpad5" || code == "Digit5"){
        grade = 5;
    }else{
        return;
    }
    
    flag++;
    console.log(flag);
    sessionStorage.setItem("trainingFlag", JSON.stringify(flag));
    TrainingPair = TrainingSet[flag];

    if(flag==1){
        window.location.replace("info2.php");
    }else if(flag<=TrainingSet.length-1){
        orig.srcset = "Images/Original/" + TrainingPair[0];
        vers.srcset = "Images/Versions/" + TrainingPair[1];
    }else{
        window.location.replace("info3.php");
    } 


  }, false);

