<?php 
require 'header.php';

?>
<html>
    <body class="bg-secondary" style="min-width:300px">

    <div class="d-flex align-items-center min-vh-100">
        <div class="jumbotron container-sm text-center p-4 mt-4 border border-dark rounded-2 w-50" onclick="start()">
            <h1>Iniciar Teste</h1>
        </div>
    </div>

    </body>
    <script>
        function shuffle(array) {
            let currentIndex = array.length,  randomIndex;

            // While there remain elements to shuffle...
            while (currentIndex != 0) {

                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex--;

                // And swap it with the current element.
                [array[currentIndex], array[randomIndex]] = [
                array[randomIndex], array[currentIndex]];
            }

            return array;
        }

        function goodShuffle(array1,array2,array3,array4){

            let finalArray = [];
            let opt = [0,1,2,3];
            let cArr = array1;
            let hArr = array2;
            let mArr = array3;
            let tArr = array4;
            let last = opt[3];

            let size = cArr.length;
            for(let i=0; i<size; i++){
                opt = [0,1,2,3];
                opt = shuffle(opt);

                while(opt[0] == last){
                    opt = shuffle(opt);
                }
                
                
                for(j=0;j<4;j++){
                    let x = Math.floor(Math.random() * cArr.length);
                    if(opt[j] == 0){
                        finalArray.push(cArr[x]);
                        cArr.splice(x,1);
                    }else if(opt[j]==1){
                        finalArray.push(hArr[x]);
                        hArr.splice(x,1);
                    }else if(opt[j]==2){
                        finalArray.push(mArr[x]);
                        mArr.splice(x,1);
                    }else{
                        finalArray.push(tArr[x]);
                        tArr.splice(x,1);
                    }

                }
                last = opt[3];
            }


        return finalArray;
        }

        function start(){

            TrArray = [["treino1.png","treino1_out.png"],["treino2.png","treino2_out.png"],["treino3.png","treino3_out.png"]];
            TrArray = shuffle(TrArray);

            carArray = [["cars.png","cars.png"],["cars.png","carsjp2_out1.png"],["cars.png","carsjp2_out2.png"],["cars.png","carsjp2_out3.png"],["cars.png","carsjp2_out4.png"],
                        ["cars.png","carsvp9_out1.png"],["cars.png","carsvp9_out2.png"],["cars.png","carsvp9_out3.png"],["cars.png","carsvp9_out4.png"],
                        ["cars.png","carshvec_out1.png"],["cars.png","carshvec_out2.png"],["cars.png","carshvec_out3.png"],["cars.png","carshvec_out4.png"]];

            horseArray = [["horses.png","horses.png"],["horses.png","horsesjp2_out1.png"],["horses.png","horsesjp2_out2.png"],["horses.png","horsesjp2_out3.png"],["horses.png","horsesjp2_out4.png"],
                          ["horses.png","horsesvp9_out1.png"],["horses.png","horsesvp9_out2.png"],["horses.png","horsesvp9_out3.png"],["horses.png","horsesvp9_out4.png"],
                          ["horses.png","horseshvec_out1.png"],["horses.png","horseshvec_out2.png"],["horses.png","horseshvec_out3.png"],["horses.png","horseshvec_out4.png"]];

            mountainArray = [["mountain.png","mountain.png"],["mountain.png","mountainjp2_out1.png"],["mountain.png","mountainjp2_out2.png"],["mountain.png","mountainjp2_out3.png"],["mountain.png","mountainjp2_out4.png"],
                             ["mountain.png","mountainvp9_out1.png"],["mountain.png","mountainvp9_out2.png"],["mountain.png","mountainvp9_out3.png"],["mountain.png","mountainvp9_out4.png"],
                             ["mountain.png","mountainhvec_out1.png"],["mountain.png","mountainhvec_out2.png"],["mountain.png","mountainhvec_out3.png"],["mountain.png","mountainhvec_out4.png"]];

            tetoArray = [["teto.png","teto.png"],["teto.png","tetojp2_out1.png"],["teto.png","tetojp2_out2.png"],["teto.png","tetojp2_out3.png"],["teto.png","tetojp2_out4.png"],
                             ["teto.png","tetovp9_out1.png"],["teto.png","tetovp9_out2.png"],["teto.png","tetovp9_out3.png"],["teto.png","tetovp9_out4.png"],
                             ["teto.png","tetohvec_out1.png"],["teto.png","tetohvec_out2.png"],["teto.png","tetohvec_out3.png"],["teto.png","tetohvec_out4.png"]];                 

            TestArray = goodShuffle(carArray,horseArray,mountainArray,tetoArray);
            
            sessionStorage.setItem("TestArray", JSON.stringify(TestArray));
            sessionStorage.setItem("testFlag", JSON.stringify(0));
            
            sessionStorage.setItem("TrainingArray", JSON.stringify(TrArray));
            sessionStorage.setItem("trainingFlag", JSON.stringify(0));
            
            window.location.replace("info1.php");
        }
    </script> 
</html>
