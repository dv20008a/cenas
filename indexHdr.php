<?php 
require 'header.php';

?>
<html>
    <body class="bg-secondary" style="min-width:300px">

    <div class="d-flex align-items-center min-vh-100">
        <div class="jumbotron container-sm text-center p-4 mt-4 border border-dark rounded-2 w-50" onclick="start()">
            <h1>Iniciar Teste</h1>
        </div>
    </div>

    </body>
    <script>
        function shuffle(array) {
            let currentIndex = array.length,  randomIndex;

            // While there remain elements to shuffle...
            while (currentIndex != 0) {

                // Pick a remaining element...
                randomIndex = Math.floor(Math.random() * currentIndex);
                currentIndex--;

                // And swap it with the current element.
                [array[currentIndex], array[randomIndex]] = [
                array[randomIndex], array[currentIndex]];
            }

            return array;
        }

        function goodShuffle(array1,array2,array3,array4){

            let finalArray = [];
            let opt = [0,1,2,3];
            let cArr = array1;
            let hArr = array2;
            let mArr = array3;
            let tArr = array4;
            let last = opt[3];

            let size = cArr.length;
            for(let i=0; i<size; i++){
                opt = [0,1,2,3];
                opt = shuffle(opt);

                while(opt[0] == last){
                    opt = shuffle(opt);
                }
                
                
                for(j=0;j<4;j++){
                    let x = Math.floor(Math.random() * cArr.length);
                    if(opt[j] == 0){
                        finalArray.push(cArr[x]);
                        cArr.splice(x,1);
                    }else if(opt[j]==1){
                        finalArray.push(hArr[x]);
                        hArr.splice(x,1);
                    }else if(opt[j]==2){
                        finalArray.push(mArr[x]);
                        mArr.splice(x,1);
                    }else{
                        finalArray.push(tArr[x]);
                        tArr.splice(x,1);
                    }

                }
                last = opt[3];
            }


        return finalArray;
        }

        function start(){

            TrArray = [["treino1.png","treino1_out.png"],["treino2.png","treino2_out.png"],["treino3.png","treino3_out.png"]];
            TrArray = shuffle(TrArray);

            img1Array = [["img1_drago_original.jpg","img1_drago_original.jpg"],["img1_drago_original.jpg","img1_drago_5q.jpg"],["img1_drago_original.jpg","img1_drago_10q.jpg"],["img1_drago_original.jpg","img1_drago_20q.jpg"],["img1_drago_original.jpg","img1_drago_70q.jpg"],
                        ["img1_durand_original.jpg","img1_durand_original.jpg"],["img1_durand_original.jpg","img1_durand_5q.jpg"],["img1_durand_original.jpg","img1_durand_10q.jpg"],["img1_durand_original.jpg","img1_durand_20q.jpg"],["img1_durand_original.jpg","img1_durand_70q.jpg"],
                        ["img1_mai_original.jpg","img1_mai_original.jpg"],["img1_mai_original.jpg","img1_mai_5q.jpg"],["img1_mai_original.jpg","img1_mai_10q.jpg"],["img1_mai_original.jpg","img1_mai_20q.jpg"],["img1_mai_original.jpg","img1_mai_70q.jpg"]];

            dragoArray = [["memorial_drago_original.jpg","memorial_drago_original.jpg"],["memorial_drago_original.jpg","memorial_drago_5q.jpg"],["memorial_drago_original.jpg","memorial_drago_10q.jpg"],["memorial_drago_original.jpg","memorial_drago_20q.jpg"],["memorial_drago_original.jpg","memorial_drago_70q.jpg"],
                          ["memorial_durand_original.jpg","memorial_durand_original.jpg"],["memorial_durand_original.jpg","memorial_durand_5q.jpg"],["memorial_durand_original.jpg","memorial_durand_10q.jpg"],["memorial_durand_original.jpg","memorial_durand_20q.jpg"],["memorial_durand_original.jpg","memorial_durand_70q.jpg"],
                          ["memorial_mai_original.jpg","memorial_mai_original.jpg"],["memorial_mai_original.jpg","memorial_mai_5q.jpg"],["memorial_mai_original.jpg","memorial_mai_10q.jpg"],["memorial_mai_original.jpg","memorial_mai_20q.jpg"],["memorial_mai_original.jpg","memorial_mai_70q.jpg"]];

            naveArray = [["nave_drago_original.jpg","nave_drago_original.jpg"],["nave_drago_original.jpg","nave_drago_5q.jpg"],["nave_drago_original.jpg","nave_drago_10q.jpg"],["nave_drago_original.jpg","nave_drago_20q.jpg"],["nave_drago_original.jpg","nave_drago_70q.jpg"],
                             ["nave_durand_original.jpg","nave_durand_original.jpg"],["nave_durand_original.jpg","nave_durand_5q.jpg"],["nave_durand_original.jpg","nave_durand_10q.jpg"],["nave_durand_original.jpg","nave_durand_20q.jpg"],["nave_durand_original.jpg","nave_durand_70q.jpg"],
                             ["nave_mai_original.jpg","nave_mai_original.jpg"],["nave_mai_original.jpg","nave_mai_5q.jpg"],["nave_mai_original.jpg","nave_mai_10q.jpg"],["nave_mai_original.jpg","nave_mai_20q.jpg"],["nave_mai_original.jpg","nave_mai_70q.jpg"]];

            rosetteArray = [["rosette_drago_original.jpg","rosette_drago_original.jpg"],["rosette_drago_original.jpg","rosette_drago_5q.jpg"],["rosette_drago_original.jpg","rosette_drago_10q.jpg"],["rosette_drago_original.jpg","rosette_drago_20q.jpg"],["rosette_drago_original.jpg","rosette_drago_70q.jpg"],
                             ["rosette_durand_original.jpg","rosette_durand_original.jpg"],["rosette_durand_original.jpg","rosette_durand_5q.jpg"],["rosette_durand_original.jpg","rosette_durand_10q.jpg"],["rosette_durand_original.jpg","rosette_durand_20q.jpg"],["rosette_durand_original.jpg","rosette_durand_70q.jpg"],
                             ["rosette_mai_original.jpg","rosette_mai_original.jpg"],["rosette_mai_original.jpg","rosette_mai_5q.jpg"],["rosette_mai_original.jpg","rosette_mai_10q.jpg"],["rosette_mai_original.jpg","rosette_mai_20q.jpg"],["rosette_mai_original.jpg","rosette_mai_70q.jpg"]];                 

            TestArray = goodShuffle(img1Array,dragoArray,naveArray,rosetteArray);
            
            sessionStorage.setItem("TestArray", JSON.stringify(TestArray));
            sessionStorage.setItem("testFlag", JSON.stringify(0));
            
            sessionStorage.setItem("TrainingArray", JSON.stringify(TrArray));
            sessionStorage.setItem("trainingFlag", JSON.stringify(0));
            
            window.location.replace("info1.php");
        }
    </script> 
</html>
